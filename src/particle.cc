#include "particle.h"
#include "camera.h"
#include <iostream>
#include <glm/gtx/string_cast.hpp>
#include <vector>
#include <math.h>       /* ceil */
using namespace std;

//https://stackoverflow.com/questions/4310277/producing-random-float-from-negative-to-positive-range
float RandomNumber(float Min, float Max)
{
    return ((float(rand()) / float(RAND_MAX)) * (Max - Min)) + Min;
}


void
ParticleSystem::boom(glm::vec3 origin, vector<glm::vec3>& offsets,  vector<float>& percent_life) {
    
    for (int i = 0; i < numParticles_; ++i) {
        Particle p;
     
        p.position = origin;
        float range = avg_speed_ / var_speed_;
        float speed = avg_speed_ + RandomNumber(-range, range) * var_speed_;

        //http://corysimon.github.io/articles/uniformdistn-on-sphere/
        float theta = 2 * M_PI * RandomNumber(0, 1);
        float phi = acos(1 - 2 * RandomNumber(0, 1));
        float x = sin(phi) * cos(theta);
        float y = abs((sin(phi) * sin(theta))) * 5;
        float z = cos(phi);
        
        p.velocity = glm::vec3(x, y, z) * speed + (wind_ * wind_speed_);
        p.lifetime = 32;
        float factor = RandomNumber(1, 2); 
        p.delta = (1/(4 * factor));
        particles[i] = p;
        percent_life[i] = p.age / p.lifetime;
        offsets[i].x = p.position.x;
        offsets[i].y = p.position.y;
        offsets[i].z = p.position.z;
    }
    
}

void
ParticleSystem::setWind(glm::vec3 wind) {
    wind_ = wind;
}
void
ParticleSystem::update(vector<glm::vec3>& offsets, vector<float>& percent_life) {
  
    int index = 0;
    for(auto& particle: particles) {
        if(particle.isAlive) {
            particle.age += particle.delta;

            //decrease life
            if(particle.age < particle.lifetime) {
                
                particle.position += particle.velocity * particle.delta + (wind_speed_ * wind_);
                
                particle.velocity +=  (glm::vec3(0.0, -9.8, 0.0) * particle.delta * 0.5f);
                if (particle.position.y < -5) {
                    particle.position.y = -5;
                }
                
            } else {
                particle.isAlive = false;
            }  
           
        }
        percent_life[index] = (particle.age / particle.lifetime);
        offsets[index].x = particle.position.x;
        offsets[index].y = particle.position.y;
        offsets[index].z = particle.position.z;
        ++index;
    }
}

void
FloorSmoke::setFloorSize(float size) {
    floor_size_ = size;
}


void
FloorSmoke::addParticle(Particle& p) {
   
    float range = avg_speed_ / var_speed_;
  
    float x_pos = RandomNumber(-floor_size_, floor_size_);
    float y_pos = -20;
    float z_pos = RandomNumber(-floor_size_, floor_size_);
  
   
    p.position = glm::vec3(x_pos, y_pos, z_pos);
    p.velocity = glm::vec3(0, .01, 0) + ( wind_ * wind_speed_);
    p.lifetime = RandomNumber(20, 100);
   
    p.delta = 0.125;
}
void 
FloorSmoke::boom(glm::vec3 origin, vector<glm::vec3>& offsets,  vector<float>& percent_life) {
    
    for (int i = 0; i < numParticles_; ++i) {
        addParticle(particles[i]);
        percent_life[i] = (particles[i].age / particles[i].lifetime);
        offsets[i].x = particles[i].position.x;
        offsets[i].y = particles[i].position.y;
        offsets[i].z = particles[i].position.z;
    }
}

void 
FloorSmoke::update(vector<glm::vec3>& offsets, vector<float>& percent_life) {
   
    int index = 0;
    for(auto& particle: particles) {
    
        particle.age += particle.delta;
        //decrease life
        if(particle.age < particle.lifetime) {
            particle.position += glm::vec3(0, .01, 0) + (wind_ * wind_speed_);
             if (particle.position.y < -5) {
                particle.position.y = -5;
            }
          
        } else {
            addParticle(particle);
            particle.age = 0;
        }     
        percent_life[index] = (particle.age / particle.lifetime);
        offsets[index].x = particle.position.x;
        offsets[index].y = particle.position.y;
        offsets[index].z = particle.position.z;  
    
        ++index;
    }
}