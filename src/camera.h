#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

class Camera {
public:
	glm::mat4 get_view_matrix() const;
	void zoom(float dir); 
	void horizontal_pan(float dir);
	void vertical_pan(float dir);
	glm::vec3 getCamera(){return eye_;};
	void rotateVectors(glm::vec3 mouse_direction);

	// FIXME: add functions to manipulate camera objects.
private:
	float camera_distance_ = 100;
	glm::vec3 look_ = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up_ = glm::vec3(0.0f, 1.0, 0.0f);
	glm::vec3 eye_ = glm::vec3(0.0f, 5, camera_distance_);	
	glm::vec3 center_ = eye_ + look_;
	glm::vec3 tangent_ = glm::cross(look_, up_);
	glm::mat3 orientation_ = glm::mat3(tangent_, up_, look_);
	// Note: you may need additional member variables
};

#endif
