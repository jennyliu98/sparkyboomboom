#ifndef PARTICLE_H
#define PARTICLE_H
#include <glm/glm.hpp>
#include <vector>
#include "camera.h"

using namespace std;
 
struct Particle {
    glm::vec3 position; //position
    glm::vec3 velocity; // speed and direction
    float lifetime;
    float age = 0;
    bool isAlive = true;
    float delta;

};

class ParticleSystem {
    public:
       ParticleSystem(int numParticles, float avg_speed, float var_speed):numParticles_(numParticles), avg_speed_(avg_speed), var_speed_(var_speed) {particles = vector<Particle>(numParticles_); }
        virtual void update(vector<glm::vec3>& offsets, vector<float>& percent_life);
        virtual void boom(glm::vec3 origin, vector<glm::vec3>& offsets,  vector<float>& percent_life);
        void setWind(glm::vec3 wind);
        float wind_speed_ = 0;
     
        // Functions
    protected:
         // Instance variables
        glm::vec3 origin;
        float avg_speed_;
        float var_speed_;
        int numParticles_;
        vector<Particle> particles;
        glm::vec3 wind_ = glm::vec3(0, 0, 0);


};

class FloorSmoke: public ParticleSystem {
    public:
        using ParticleSystem::ParticleSystem;
        void boom(glm::vec3 origin, vector<glm::vec3>& offsets,  vector<float>& percent_life);
        void update(vector<glm::vec3>& offsets, vector<float>& percent_life);
        void addParticle(Particle& p);
        void setFloorSize(float size);
    private:
        float floor_size_;

};

#endif