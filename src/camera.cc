#include "camera.h"
 #include <glm/gtc/matrix_transform.hpp>
 #include <iostream>
 #include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
using namespace std;
namespace {
	float pan_speed = 0.1f;
	float roll_speed = 0.1f;
	float rotation_speed_ = 0.02f;
	float zoom_speed = 1.0f;
};

// FIXME: Calculate the view matrix
glm::mat4 Camera::get_view_matrix() const
{
	glm::mat4 view_matrix = glm::mat4(1.0);
	view_matrix[0][0] = tangent_.x;
	view_matrix[1][0] = tangent_.y;
	view_matrix[2][0] = tangent_.z;
	view_matrix[3][0] = glm::dot(tangent_, -eye_);

	view_matrix[0][1] = up_.x;
	view_matrix[1][1] = up_.y;
	view_matrix[2][1] = up_.z;
	view_matrix[3][1] = glm::dot(up_, -eye_);

	view_matrix[0][2] = -look_.x;
	view_matrix[1][2] = -look_.y;
	view_matrix[2][2] = -look_.z;
	view_matrix[3][2] = glm::dot(-look_, -eye_);

	view_matrix[0][3] = 0;
	view_matrix[1][3] = 0;
	view_matrix[2][3] = 0;
	view_matrix[3][3] = 1;

	return view_matrix;	
}
bool checkBoundary(glm::vec3 pos) {
	int size = 150;
	return pos.x <= size && pos.x >= -size &&  pos.z <= size && pos.z >= -size && pos.y >= -2;
}
void Camera::zoom(float dir) 
{
	float eye_zoom = dir * zoom_speed;
	glm::vec3 new_eye = eye_ -(eye_zoom * look_);
	
	if(checkBoundary(new_eye)) {
		eye_ = new_eye;	
		camera_distance_ += dir * zoom_speed;
		center_ = eye_ + look_ * camera_distance_;
	}
	

}

void Camera::horizontal_pan(float dir) 
{
	float eye_pan = dir * pan_speed;
	glm::vec3 new_eye = eye_ + (eye_pan * tangent_);	
	if(checkBoundary(new_eye)) {
		eye_ = new_eye;	
	}

}

void Camera::vertical_pan(float dir) 
{
	float eye_pan = dir * pan_speed;
	glm::vec3 new_eye = eye_ + (eye_pan * up_);	
	if(checkBoundary(new_eye)) {
		eye_ = new_eye;	
	}
}

void Camera::rotateVectors(glm::vec3 mouse_direction) {
	glm::vec3 axis = glm::normalize(
				orientation_ *
				glm::vec3(mouse_direction.y, -mouse_direction.x, 0.0f)
				);
	orientation_ =
		glm::mat3(glm::rotate(rotation_speed_, axis) * glm::mat4(orientation_));
	tangent_ = glm::column(orientation_, 0);
	up_ = glm::column(orientation_, 1);
	look_ = glm::column(orientation_, 2);
	center_ = eye_ + camera_distance_ * look_;
}

