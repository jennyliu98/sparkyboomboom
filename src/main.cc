#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

// OpenGL library includes
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <debuggl.h>
#include "camera.h"
#include "particle.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stbi.h"

const char* vertex_shader = 
#include "Shaders/obj.vert"
;

const char* fragment_shader =
#include "Shaders/obj.frag"
;

const char* floor_vertex_shader =
#include "Shaders/floor.vert"
;

const char* floor_geometry_shader =
#include "Shaders/floor.geom"
;

const char* floor_fragment_shader =
#include "Shaders/floor.frag"
;

const char* tessellation_eval_shader =
#include "Shaders/floor.tesseval"
;

const char* tessellation_control_shader =
#include "Shaders/floor.tessctrl"
;

const char* floor_smoke_vertex_shader = 
#include "Shaders/floorsmoke.vert"
;

const char* floor_smoke_fragment_shader =
#include "Shaders/floorsmoke.frag"
;

const char* sky_box_vertex_shader = 
#include "Shaders/skybox.vert"
;

const char* sky_box_fragment_shader =
#include "Shaders/skybox.frag"
;


using namespace std;

int window_width = 1280, window_height = 720;

// VBO and VAO descriptors.
enum { kVertexBuffer, kIndexBuffer, kTranslationBuffer, kLifeBuffer, kNumVbos };

// These are our VAOs.
enum { kGeometryVao, kFloorVao, kFloorSmokeVao, kSkyBoxVao, kNumVaos };

GLuint g_array_objects[kNumVaos];  // This will store the VAO descriptors.
GLuint g_buffer_objects[kNumVaos][kNumVbos];  // These will store VBO descriptors.


unsigned int loadTexture(string texture)
{
    unsigned int texture_location;
    glGenTextures(1, &texture_location);
   	glBindTexture(GL_TEXTURE_2D, texture_location);

    int width, height, nrChannels;

	
	unsigned char *data = stbi_load(texture.c_str(), &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	}
	
	stbi_image_free(data);
  
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   
    return texture_location;
}  

// Textures used for geometries
const string atlas = "../explosion2.jpg";
const string lava = "../lava_floor.jpg";
const string floor_smoke = "../smoke_ball.jpg";

unsigned int loadCubemap(vector<std::string> faces)
{
    unsigned int texture_location;
    glGenTextures(1, &texture_location);
   	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_location);

    int width, height, nrChannels;

    for (unsigned int i = 0; i < faces.size(); i++)
    {
		
        unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 
                         0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
            );
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return texture_location;
}  

// Textures used for the skybox
vector<std::string> faces
{
	"../mp_mandaris/mandaris_ft.tga",
	"../mp_mandaris/mandaris_bk.tga",
	"../mp_mandaris/mandaris_up.tga",
    "../mp_mandaris/mandaris_dn.tga",
	"../mp_mandaris/mandaris_rt.tga",
	"../mp_mandaris/mandaris_lf.tga"
};

void
ErrorCallback(int error, const char* description)
{
	std::cerr << "GLFW Error: " << description << "\n";
}

Camera g_camera;

int num_particles = 100;
std::vector<glm::vec3> offsets(num_particles);
std::vector<float> percent_life(num_particles);
glm::vec3 explosion_origin;
ParticleSystem p_system(num_particles, 10.0f, 10.0f);
int num_smokes = 100;
FloorSmoke smoke(num_smokes, .1, 0.0001);

bool paused = false;
float boom = 0.0;
float max_wind = 10;
float wind_speed = 0;
float wind_delta = 0.05;
void
KeyCallback(GLFWwindow* window,
            int key,
            int scancode,
            int action,
            int mods)
{
	// Note:
	// This is only a list of functions to implement.
	// you may want to re-organize this piece of code.
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	else if (key == GLFW_KEY_W && action != GLFW_RELEASE) {
		g_camera.zoom(-1);
	} else if (key == GLFW_KEY_S && action != GLFW_RELEASE) {
		g_camera.zoom(1);
	} else if (key == GLFW_KEY_A && action != GLFW_RELEASE) {
		g_camera.horizontal_pan(-1);
	} else if (key == GLFW_KEY_D && action != GLFW_RELEASE) {
		g_camera.horizontal_pan(1);
	} else if (key == GLFW_KEY_LEFT && action != GLFW_RELEASE) {
		g_camera.horizontal_pan(-1);
		
	} else if (key == GLFW_KEY_RIGHT && action != GLFW_RELEASE) {
		g_camera.horizontal_pan(1);
	} else if (key == GLFW_KEY_DOWN && action != GLFW_RELEASE) {
		g_camera.vertical_pan(-1);
	} else if (key == GLFW_KEY_UP && action != GLFW_RELEASE) {
		g_camera.vertical_pan(1);
	} else if(key == GLFW_KEY_SPACE && action != GLFW_RELEASE) {
		paused = !paused;
	} else if(key == GLFW_KEY_B && action != GLFW_RELEASE) {
		p_system.boom(glm::vec3(0, -2, 0), offsets, percent_life);
		boom = 1.0;
	} else if(key == GLFW_KEY_RIGHT_BRACKET && action != GLFW_RELEASE) { //increase wind speed
		if (wind_speed < max_wind) {
			wind_speed += wind_delta;
			smoke.wind_speed_ = wind_speed;
			p_system.wind_speed_ = wind_speed;	
		}
	} else if(key == GLFW_KEY_LEFT_BRACKET && action != GLFW_RELEASE) { //decrease wind speed
		if (wind_speed >= 0.05) {
			wind_speed -= wind_delta;
			smoke.wind_speed_ = wind_speed;
			p_system.wind_speed_ = wind_speed;
		}
	}	
}

int g_current_button;
bool g_mouse_pressed;
float last_x_ = 0.0f, last_y_ = 0.0f, current_x_ = 0.0f, current_y_ = 0.0f;
void
MousePosCallback(GLFWwindow* window, double mouse_x, double mouse_y)
{
	last_x_ = current_x_;
	last_y_ = current_y_;
	current_x_ = mouse_x;
	current_y_ = window_height - mouse_y;
	if (!g_mouse_pressed) {
		
		return;
	}		
	if (g_current_button == GLFW_MOUSE_BUTTON_LEFT) {
		// FIXME: left drag
		float delta_x = current_x_ - last_x_;
		float delta_y = current_y_ - last_y_;
		if (sqrt(delta_x * delta_x + delta_y * delta_y) < 1e-15)
			return;
		if (mouse_x > window_width)
			return ;
		glm::vec3 mouse_direction = -glm::normalize(glm::vec3(delta_x, delta_y, 0.0f));
		g_camera.rotateVectors(mouse_direction);

	} else if (g_current_button == GLFW_MOUSE_BUTTON_RIGHT) { //WIND

		float delta_x = current_x_ - last_x_;
		float delta_y = current_y_ - last_y_;

		// Compute the projection matrix.
		float aspect = static_cast<float>(window_width) / window_height;
		glm::mat4 projection_matrix_ =
			glm::perspective(glm::radians(45.0f), aspect, 0.0001f, 1000.0f);
		glm::mat4 view_matrix_ = g_camera.get_view_matrix();

		// convert mouse drag vector from screen to world coordinates
		float mouse_x_ndc_current = (2.0f * current_x_) / (window_width) - 1;
		float mouse_y_ndc_current = (2.0f * current_y_) / (window_height) - 1;
		glm::vec4 mouse_current = glm::vec4(mouse_x_ndc_current, mouse_y_ndc_current, 0, 1);
		glm::vec4 mouse_camera_current = glm::inverse(projection_matrix_) * mouse_current;
		mouse_camera_current /= mouse_camera_current.w;
		glm::vec4 mouse_world_current = glm::inverse(view_matrix_) * mouse_camera_current;
		float mouse_x_ndc_last = (2.0f * last_x_) / (window_width) - 1;
		float mouse_y_ndc_last = (2.0f * last_y_) / (window_height) - 1;
		glm::vec4 mouse_last = glm::vec4(mouse_x_ndc_last, mouse_y_ndc_last, 0, 1);
		glm::vec4 mouse_camera_last = glm::inverse(projection_matrix_) * mouse_last;
		mouse_camera_last /= mouse_camera_last.w;
		glm::vec4 mouse_world_last = glm::inverse(view_matrix_) * mouse_camera_last;
		glm::vec3 wind = glm::vec3(glm::normalize(mouse_world_current - mouse_world_last));

		if(!glm::isnan(wind.x) && !glm::isnan(wind.y) && !glm::isnan(wind.z)) { //idk. strangely nan sometimes? 
			smoke.setWind(wind);
		}	
	}
}


void
MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	
	g_mouse_pressed = (action == GLFW_PRESS);
	g_current_button = button;

}

void
CreateTriangle(std::vector<glm::vec4>& vertices,
        std::vector<glm::uvec3>& indices, glm::dvec4 v1, glm::dvec4 v2, glm::dvec4 v3, int& index)
{
	
	vertices.push_back(v1);
	vertices.push_back(v2);
	vertices.push_back(v3);

	indices.push_back(glm::uvec3(index, index+1, index+2));
	index += 3;
}

void 
createFloor (std::vector<glm::vec4>& vertices,
        std::vector<glm::uvec3>& indices) 
{
	float horizon_line = -5;
	float size = 200;
	glm::vec4 v1 = glm::vec4(-size, horizon_line, -size, 1);
	glm::vec4 v2 = glm::vec4(-size, horizon_line, size, 1);
	glm::vec4 v3 = glm::vec4(size, horizon_line, -size, 1);
	glm::vec4 v4 = glm::vec4(size, horizon_line, size, 1);
	int index = 0;
	CreateTriangle(vertices, indices, v3, v1, v2, index);
	CreateTriangle(vertices, indices, v2, v4, v3, index);
	smoke.setFloorSize(size);
}

void 
CreateQuad(float size, std::vector<glm::vec4>& vertices,std::vector<glm::uvec3>& indices) {
	
	int index = 0;

	glm::vec4 v1 = glm::vec4(-size, size, 0, 1);
	glm::vec4 v2 = glm::vec4(-size, -size, 0, 1);
	glm::vec4 v3 = glm::vec4(size, -size, 0, 1);
	CreateTriangle(vertices, indices, v1, v2, v3, index);

	glm::vec4 v4 = glm::vec4(size, size, 0, 1);
	
	CreateTriangle(vertices, indices, v4, v1, v3, index);
}

void 
createCube (std::vector<glm::vec4>& vertices,
        std::vector<glm::uvec3>& indices) 
{

	glm::vec4 min = glm::vec4(-0.5, -0.5, -0.5, 1);
	glm::vec4 max = glm::vec4(0.5, 0.5, 0.5, 1);

	glm::vec4 v1 = min;
	glm::vec4 v2 = glm::vec4(min.x, max.y, min.z, 1);
	glm::vec4 v3 = glm::vec4(max.x, min.y, min.z, 1);
	glm::vec4 v4 = glm::vec4(max.x, max.y, min.z, 1);
	glm::vec4 v5 = glm::vec4(min.x, min.y, max.z, 1);
	glm::vec4 v6 = glm::vec4(min.x, max.y, max.z, 1);
	glm::vec4 v7 = glm::vec4(max.x, min.y, max.z, 1);
	glm::vec4 v8 = max;

	//front face
	int index = 0;
	CreateTriangle(vertices, indices, v6, v5, v7, index);
	CreateTriangle(vertices, indices, v8, v6, v7, index);
	
	//back face
	CreateTriangle(vertices, indices, v1, v2, v4, index);
	CreateTriangle(vertices, indices, v4, v3, v1, index);

	//bottom face
	CreateTriangle(vertices, indices, v7, v5, v1, index);
	CreateTriangle(vertices, indices, v7, v1, v3, index);
	
	//left face
	CreateTriangle(vertices, indices, v2, v1, v6, index);
	CreateTriangle(vertices, indices, v6, v1, v5, index);
	

	//right face
	CreateTriangle(vertices, indices, v8, v3, v4, index);
	CreateTriangle(vertices, indices, v3, v8, v7, index);

	
	//top face
	CreateTriangle(vertices, indices, v8, v4, v2, index);
	CreateTriangle(vertices, indices, v8, v2, v6, index);
}


int 
main(int argc, char* argv[])
{
	std::string window_title = "sparkyboomboom";
	if (!glfwInit()) exit(EXIT_FAILURE);
	glfwSetErrorCallback(ErrorCallback);

	// Ask an OpenGL 4.1 core profile context
	// It is required on OSX and non-NVIDIA Linux
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 16);
	GLFWwindow* window = glfwCreateWindow(window_width, window_height,
			&window_title[0], nullptr, nullptr);
	CHECK_SUCCESS(window != nullptr);
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;

	CHECK_SUCCESS(glewInit() == GLEW_OK);
	glGetError();  // clear GLEW's error for it
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetCursorPosCallback(window, MousePosCallback);
	glfwSetMouseButtonCallback(window, MouseButtonCallback);
	glfwSwapInterval(1);
	const GLubyte* renderer = glGetString(GL_RENDERER);  // get renderer string
	const GLubyte* version = glGetString(GL_VERSION);    // version as a string
	std::cout << "Renderer: " << renderer << "\n";
	std::cout << "OpenGL version supported:" << version << "\n";

	// Set up obj geometry
	std::vector<glm::vec4> obj_vertices;
	std::vector<glm::uvec3> obj_faces;
	CreateQuad(60, obj_vertices, obj_faces);

	// Set up floor geometry
	std::vector<glm::vec4> floor_vertices;
	std::vector<glm::uvec3> floor_faces;
	createFloor(floor_vertices, floor_faces);


	//set up floor smoke geometry
	std::vector<glm::vec4> fsmoke_vertices;
	std::vector<glm::uvec3> fsmoke_faces;
	CreateQuad(60, fsmoke_vertices, fsmoke_faces);

	//floor smoke offsets
	std::vector<glm::vec3> fsmoke_offsets(num_smokes);
	std::vector<float> fsmoke_percent_life(num_smokes);
	smoke.boom(glm::vec3(0,0,0), fsmoke_offsets, fsmoke_percent_life);

	//set up skybox
	std::vector<glm::vec4> cube_vertices;
	std::vector<glm::uvec3> cube_faces;
	createCube(cube_vertices, cube_faces);

	// Setup our VAO array.
	CHECK_GL_ERROR(glGenVertexArrays(kNumVaos, &g_array_objects[0]));

	// Switch to the VAO for Geometry.
	CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kGeometryVao]));

	// Generate buffer objects 
	CHECK_GL_ERROR(glGenBuffers(kNumVbos, &g_buffer_objects[kGeometryVao][0]));

	// Setup vertex data in a VBO.
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kGeometryVao][kVertexBuffer]));
	// NOTE: We do not send anything right now, we just describe it to OpenGL.
	CHECK_GL_ERROR(glBufferData(GL_ARRAY_BUFFER,
				sizeof(float) * obj_vertices.size() * 4, obj_vertices.data(),
				GL_STATIC_DRAW));
	CHECK_GL_ERROR(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0));
	CHECK_GL_ERROR(glEnableVertexAttribArray(0));

	// Setup element array buffer.
	CHECK_GL_ERROR(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_buffer_objects[kGeometryVao][kIndexBuffer]));
	CHECK_GL_ERROR(glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				sizeof(uint32_t) * obj_faces.size() * 3,
				obj_faces.data(), GL_STATIC_DRAW));

	//offsets
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kGeometryVao][kTranslationBuffer]));
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * offsets.size(), &offsets[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glVertexAttribDivisor(1, 1);

	//percent life
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kGeometryVao][kLifeBuffer]));
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * percent_life.size(), &percent_life[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float), (void*)0);
	glVertexAttribDivisor(2, 1);

	// Setup vertex shader.
	GLuint vertex_shader_id = 0;
	const char* vertex_source_pointer = vertex_shader;
	CHECK_GL_ERROR(vertex_shader_id = glCreateShader(GL_VERTEX_SHADER));
	CHECK_GL_ERROR(glShaderSource(vertex_shader_id, 1, &vertex_source_pointer, nullptr));
	glCompileShader(vertex_shader_id);
	CHECK_GL_SHADER_ERROR(vertex_shader_id);


	// Setup fragment shader.
	GLuint fragment_shader_id = 0;
	const char* fragment_source_pointer = fragment_shader;
	CHECK_GL_ERROR(fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER));
	CHECK_GL_ERROR(glShaderSource(fragment_shader_id, 1, &fragment_source_pointer, nullptr));
	glCompileShader(fragment_shader_id);
	CHECK_GL_SHADER_ERROR(fragment_shader_id);

	// Let's create our program.
	GLuint program_id = 0;
	CHECK_GL_ERROR(program_id = glCreateProgram());
	CHECK_GL_ERROR(glAttachShader(program_id, vertex_shader_id));
	CHECK_GL_ERROR(glAttachShader(program_id, fragment_shader_id));
	//CHECK_GL_ERROR(glAttachShader(program_id, geometry_shader_id));

	// Bind attributes.
	CHECK_GL_ERROR(glBindAttribLocation(program_id, 0, "vertex_position"));
	CHECK_GL_ERROR(glBindAttribLocation(program_id, 1, "offset"));
	CHECK_GL_ERROR(glBindAttribLocation(program_id, 2, "life"));
	CHECK_GL_ERROR(glBindFragDataLocation(program_id, 0, "fragment_color")); glDepthMask( GL_FALSE ); // Disable depth writes
   		glEnable(GL_BLEND);                 // Enable Blending
    	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );   // Type Of Blending To Perform
	glLinkProgram(program_id);
	CHECK_GL_PROGRAM_ERROR(program_id);

	// Get the uniform locations.
	GLint projection_matrix_location = 0;
	CHECK_GL_ERROR(projection_matrix_location =
			glGetUniformLocation(program_id, "projection"));
	GLint view_matrix_location = 0;
	CHECK_GL_ERROR(view_matrix_location =
			glGetUniformLocation(program_id, "view"));
	

	///////////////////////////////////// SET UP FLOOR /////////////////////////////////////////////
	// Switch to the VAO for Floor.
	CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kFloorVao]));

	// Generate buffer objects
	CHECK_GL_ERROR(glGenBuffers(kNumVbos, &g_buffer_objects[kFloorVao][0]));

	// Setup vertex data in a VBO.
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kFloorVao][kVertexBuffer]));
	// NOTE: We do not send anything right now, we just describe it to OpenGL.
	CHECK_GL_ERROR(glBufferData(GL_ARRAY_BUFFER,
				sizeof(float) * floor_vertices.size() * 4, floor_vertices.data(),
				GL_STATIC_DRAW));
	CHECK_GL_ERROR(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0));
	CHECK_GL_ERROR(glEnableVertexAttribArray(0));

	// Setup element array buffer.
	CHECK_GL_ERROR(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_buffer_objects[kFloorVao][kIndexBuffer]));
	CHECK_GL_ERROR(glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				sizeof(uint32_t) * floor_faces.size() * 3,
				floor_faces.data(), GL_STATIC_DRAW));

	// Setup floor vertex shader.
	GLuint floor_vertex_shader_id = 0;
	const char* floor_vertex_source_pointer = floor_vertex_shader;
	CHECK_GL_ERROR(floor_vertex_shader_id = glCreateShader(GL_VERTEX_SHADER));
	CHECK_GL_ERROR(glShaderSource(floor_vertex_shader_id, 1, &floor_vertex_source_pointer, nullptr));
	glCompileShader(floor_vertex_shader_id);
	CHECK_GL_SHADER_ERROR(floor_vertex_shader_id);

	// Setup floor tessellation_control_shader.
	GLuint tessellation_control_shader_id = 0;
	const char* tessellation_control_source_pointer = tessellation_control_shader;
	CHECK_GL_ERROR(tessellation_control_shader_id = glCreateShader(GL_TESS_CONTROL_SHADER));
	CHECK_GL_ERROR(glShaderSource(tessellation_control_shader_id, 1, &tessellation_control_source_pointer, nullptr));
	glCompileShader(tessellation_control_shader_id);
	CHECK_GL_SHADER_ERROR(tessellation_control_shader_id);

	// Setup floor tessellation_eval_shader.
	GLuint tessellation_eval_shader_id = 0;
	const char* tessellation_eval_source_pointer = tessellation_eval_shader;
	CHECK_GL_ERROR(tessellation_eval_shader_id = glCreateShader(GL_TESS_EVALUATION_SHADER));
	CHECK_GL_ERROR(glShaderSource(tessellation_eval_shader_id, 1, &tessellation_eval_source_pointer, nullptr));
	glCompileShader(tessellation_eval_shader_id);
	CHECK_GL_SHADER_ERROR(tessellation_eval_shader_id);

	// Setup floor geometry shader.
	GLuint floor_geometry_shader_id = 0;
	const char* floor_geometry_source_pointer = floor_geometry_shader;
	CHECK_GL_ERROR(floor_geometry_shader_id = glCreateShader(GL_GEOMETRY_SHADER));
	CHECK_GL_ERROR(glShaderSource(floor_geometry_shader_id, 1, &floor_geometry_source_pointer, nullptr));
	glCompileShader(floor_geometry_shader_id);
	CHECK_GL_SHADER_ERROR(floor_geometry_shader_id);

	// Setup floor fragment shader.
	GLuint floor_fragment_shader_id = 0;
	const char* floor_fragment_source_pointer = floor_fragment_shader;
	CHECK_GL_ERROR(floor_fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER));
	CHECK_GL_ERROR(glShaderSource(floor_fragment_shader_id, 1, &floor_fragment_source_pointer, nullptr));
	glCompileShader(floor_fragment_shader_id);
	CHECK_GL_SHADER_ERROR(floor_fragment_shader_id);

	// Let's create our floor program.
	GLuint floor_program_id = 0;
	
	CHECK_GL_ERROR(floor_program_id = glCreateProgram());
	CHECK_GL_ERROR(glAttachShader(floor_program_id, floor_vertex_shader_id));
	CHECK_GL_ERROR(glAttachShader(floor_program_id, floor_fragment_shader_id));
	CHECK_GL_ERROR(glAttachShader(floor_program_id, tessellation_control_shader_id));
	CHECK_GL_ERROR(glAttachShader(floor_program_id, tessellation_eval_shader_id));
	CHECK_GL_ERROR(glAttachShader(floor_program_id, floor_geometry_shader_id));

	// Bind attributes.
	CHECK_GL_ERROR(glBindAttribLocation(floor_program_id, 0, "vertex_position"));
	CHECK_GL_ERROR(glBindFragDataLocation(floor_program_id, 0, "fragment_color"));
	glLinkProgram(floor_program_id);
	CHECK_GL_PROGRAM_ERROR(floor_program_id);

	// Get the uniform locations.
	GLint floor_projection_matrix_location = 0;
	CHECK_GL_ERROR(floor_projection_matrix_location =
			glGetUniformLocation(floor_program_id, "projection"));
	GLint floor_view_matrix_location = 0;
	CHECK_GL_ERROR(floor_view_matrix_location =
			glGetUniformLocation(floor_program_id, "view"));
	
	GLint floor_time_location = 0;
	CHECK_GL_ERROR(floor_time_location =
			glGetUniformLocation(floor_program_id, "time"));

	
	GLint floor_boom_location = 0;
	CHECK_GL_ERROR(floor_boom_location =
			glGetUniformLocation(floor_program_id, "boom"));

	//////////////////////////////////////////////////SET UP FLOOR SMOKE ///////////////////////////////////////////////////
	// Switch to the VAO for Geometry.
	CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kFloorSmokeVao]));

	// Generate buffer objects 
	CHECK_GL_ERROR(glGenBuffers(kNumVbos, &g_buffer_objects[kFloorSmokeVao][0]));

	// Setup vertex data in a VBO.
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kFloorSmokeVao][kVertexBuffer]));
	// NOTE: We do not send anything right now, we just describe it to OpenGL.
	CHECK_GL_ERROR(glBufferData(GL_ARRAY_BUFFER,
				sizeof(float) * fsmoke_vertices.size() * 4, fsmoke_vertices.data(),
				GL_STATIC_DRAW));
	CHECK_GL_ERROR(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0));
	CHECK_GL_ERROR(glEnableVertexAttribArray(0));

	// Setup element array buffer.
	CHECK_GL_ERROR(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_buffer_objects[kFloorSmokeVao][kIndexBuffer]));
	CHECK_GL_ERROR(glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				sizeof(uint32_t) * fsmoke_faces.size() * 3,
				fsmoke_faces.data(), GL_STATIC_DRAW));

	//offsets
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kFloorSmokeVao][kTranslationBuffer]));
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * fsmoke_offsets.size(), &fsmoke_offsets[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glVertexAttribDivisor(1, 1);

	//percent life
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kFloorSmokeVao][kLifeBuffer]));
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * fsmoke_percent_life.size(), &fsmoke_percent_life[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float), (void*)0);
	glVertexAttribDivisor(2, 1);

	// Setup vertex shader.
	GLuint fsmoke_vertex_shader_id = 0;
	const char* fsmoke_vertex_source_pointer = floor_smoke_vertex_shader;
	CHECK_GL_ERROR(fsmoke_vertex_shader_id = glCreateShader(GL_VERTEX_SHADER));
	CHECK_GL_ERROR(glShaderSource(fsmoke_vertex_shader_id, 1, &fsmoke_vertex_source_pointer, nullptr));
	glCompileShader(fsmoke_vertex_shader_id);
	CHECK_GL_SHADER_ERROR(fsmoke_vertex_shader_id);

	// Setup fragment shader.
	GLuint fsmoke_fragment_shader_id = 0;
	const char* fsmoke_fragment_source_pointer = floor_smoke_fragment_shader;
	CHECK_GL_ERROR(fsmoke_fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER));
	CHECK_GL_ERROR(glShaderSource(fsmoke_fragment_shader_id, 1, &fsmoke_fragment_source_pointer, nullptr));
	glCompileShader(fsmoke_fragment_shader_id);
	CHECK_GL_SHADER_ERROR(fsmoke_fragment_shader_id);

	// Let's create our program.
	GLuint fsmoke_program_id = 0;
	CHECK_GL_ERROR(fsmoke_program_id = glCreateProgram());
	CHECK_GL_ERROR(glAttachShader(fsmoke_program_id, fsmoke_vertex_shader_id));
	CHECK_GL_ERROR(glAttachShader(fsmoke_program_id, fsmoke_fragment_shader_id));


	// Bind attributes.
	CHECK_GL_ERROR(glBindAttribLocation(fsmoke_program_id, 0, "vertex_position"));
	CHECK_GL_ERROR(glBindAttribLocation(fsmoke_program_id, 1, "offset"));
	CHECK_GL_ERROR(glBindAttribLocation(fsmoke_program_id, 2, "life"));
	CHECK_GL_ERROR(glBindFragDataLocation(fsmoke_program_id, 0, "fragment_color")); 
	glLinkProgram(fsmoke_program_id);
	CHECK_GL_PROGRAM_ERROR(fsmoke_program_id);

	// Get the uniform locations.
	GLint fsmoke_projection_matrix_location = 0;
	CHECK_GL_ERROR(fsmoke_projection_matrix_location =
			glGetUniformLocation(fsmoke_program_id, "projection"));
	GLint  fsmoke_view_matrix_location = 0;
	CHECK_GL_ERROR(fsmoke_view_matrix_location =
			glGetUniformLocation(fsmoke_program_id, "view"));


	//////////////////////////////////////////////////SET UP SKY BOX ///////////////////////////////////////////////////
	// Switch to the VAO for Geometry.
	CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kSkyBoxVao]));

	// Generate buffer objects 
	CHECK_GL_ERROR(glGenBuffers(kNumVbos, &g_buffer_objects[kSkyBoxVao][0]));

	// Setup vertex data in a VBO.
	CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kSkyBoxVao][kVertexBuffer]));
	// NOTE: We do not send anything right now, we just describe it to OpenGL.
	CHECK_GL_ERROR(glBufferData(GL_ARRAY_BUFFER,
				sizeof(float) * cube_vertices.size() * 4, cube_vertices.data(),
				GL_STATIC_DRAW));
	CHECK_GL_ERROR(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0));
	CHECK_GL_ERROR(glEnableVertexAttribArray(0));

	// Setup element array buffer.
	CHECK_GL_ERROR(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_buffer_objects[kSkyBoxVao][kIndexBuffer]));
	CHECK_GL_ERROR(glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				sizeof(uint32_t) * cube_faces.size() * 3,
				cube_faces.data(), GL_STATIC_DRAW));
	
	// Setup vertex shader.
	GLuint skybox_vertex_shader_id = 0;
	const char* skybox_vertex_source_pointer = sky_box_vertex_shader;
	CHECK_GL_ERROR(skybox_vertex_shader_id = glCreateShader(GL_VERTEX_SHADER));
	CHECK_GL_ERROR(glShaderSource(skybox_vertex_shader_id, 1, &skybox_vertex_source_pointer, nullptr));
	glCompileShader(skybox_vertex_shader_id);
	CHECK_GL_SHADER_ERROR(skybox_vertex_shader_id);

	// Setup fragment shader.
	GLuint skybox_fragment_shader_id = 0;
	const char*  skybox_fragment_source_pointer =  sky_box_fragment_shader;
	CHECK_GL_ERROR(skybox_fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER));
	CHECK_GL_ERROR(glShaderSource(skybox_fragment_shader_id, 1, &skybox_fragment_source_pointer, nullptr));
	glCompileShader(skybox_fragment_shader_id);
	CHECK_GL_SHADER_ERROR(skybox_fragment_shader_id);

	// Let's create our program.
	GLuint skybox_program_id = 0;
	CHECK_GL_ERROR(skybox_program_id = glCreateProgram());
	CHECK_GL_ERROR(glAttachShader(skybox_program_id, skybox_vertex_shader_id));
	CHECK_GL_ERROR(glAttachShader(skybox_program_id, skybox_fragment_shader_id));


	// Bind attributes.
	CHECK_GL_ERROR(glBindAttribLocation(skybox_program_id, 0, "vertex_position"));
	CHECK_GL_ERROR(glBindFragDataLocation(skybox_program_id, 0, "fragment_color")); 
	glLinkProgram(skybox_program_id);
	CHECK_GL_PROGRAM_ERROR(skybox_program_id);

	// Get the uniform locations.
	GLint skybox_projection_matrix_location = 0;
	CHECK_GL_ERROR(skybox_projection_matrix_location =
			glGetUniformLocation(skybox_program_id, "projection"));
	GLint  skybox_view_matrix_location = 0;
	CHECK_GL_ERROR(skybox_view_matrix_location =
			glGetUniformLocation(skybox_program_id, "view"));

	GLint atlas_location = loadTexture(atlas);
	GLint texture_location = loadTexture(lava);
	GLint smoke_location = loadTexture(floor_smoke);
	GLint cubemap_texture_location = loadCubemap(faces);

	float aspect = 0.0f;
	float theta = 0.0f;
	float t = 0;
	int waves = 32;
	float increment = M_PI / (waves/2.0);

	int count = 0;
	while (!glfwWindowShouldClose(window)) {
		// Setup some basic window stuff.
		glfwGetFramebufferSize(window, &window_width, &window_height);
		glViewport(0, 0, window_width, window_height);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		 glDepthMask( GL_FALSE );            // Disable depth writes
   		glEnable(GL_BLEND);                 // Enable Blending
    	//glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );   // Type Of Blending To Perform
		glBlendFunc( GL_SRC_ALPHA, GL_ONE );
		if(!paused) {
			p_system.update(offsets, percent_life);
			//update the smoke particles
			smoke.update(fsmoke_offsets, fsmoke_percent_life);
		}
		
		if(offsets.size() == 0) {
			boom = 0.0;
		}

		// Switch to the Geometry VAO.
		CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kGeometryVao]));
		
		// Compute the projection matrix.
		aspect = static_cast<float>(window_width) / window_height;
		glm::mat4 projection_matrix =
			glm::perspective(glm::radians(45.0f), aspect, 0.0001f, 1000.0f);

		// Compute the view matrix
		// FIXME: change eye and center through mouse/keyboard events.
		
		glm::mat4 view_matrix = g_camera.get_view_matrix();
		//billboard(view_matrix, obj_vertices);

		CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kGeometryVao][kTranslationBuffer]));
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * offsets.size(), &offsets[0], GL_STATIC_DRAW);

		CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kGeometryVao][kLifeBuffer]));
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * percent_life.size(), &percent_life[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0); 

		// Use our program.
		CHECK_GL_ERROR(glUseProgram(program_id));

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, atlas_location);

		// Pass uniforms in.
		CHECK_GL_ERROR(glUniformMatrix4fv(projection_matrix_location, 1, GL_FALSE,
					&projection_matrix[0][0]));
		CHECK_GL_ERROR(glUniformMatrix4fv(view_matrix_location, 1, GL_FALSE,
					&view_matrix[0][0]));
		
		// Draw our triangles.
		CHECK_GL_ERROR(glDrawElementsInstanced(GL_TRIANGLES, obj_faces.size() * 3, GL_UNSIGNED_INT, 0, offsets.size()));

		//////////////////////////// RENDER THE FLOOR ///////////////////////////
		CHECK_GL_ERROR(glPatchParameteri(GL_PATCH_VERTICES, 3));
		CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kFloorVao]));

		// Use our program.
		CHECK_GL_ERROR(glUseProgram(floor_program_id));
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_location);
		
		// Pass uniforms in.
		CHECK_GL_ERROR(glUniformMatrix4fv(floor_projection_matrix_location, 1, GL_FALSE,
					&projection_matrix[0][0]));
		CHECK_GL_ERROR(glUniformMatrix4fv(floor_view_matrix_location, 1, GL_FALSE,
					&view_matrix[0][0]));
		CHECK_GL_ERROR(glUniform1fv(floor_time_location, 1, &t));
		CHECK_GL_ERROR(glUniform1fv(floor_boom_location, 1, &boom));
		t += increment;
		// Draw our triangles.
		//draw floor 
		CHECK_GL_ERROR(glDrawElements(GL_PATCHES, floor_faces.size() * 3, GL_UNSIGNED_INT, 0));

		/////////////////////////////RENDER THE FLOOR SMOKE////////////////////////
		//Switch to the Geometry VAO.
		CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kFloorSmokeVao]));
		
		CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kFloorSmokeVao][kTranslationBuffer]));
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * fsmoke_offsets.size(), &fsmoke_offsets[0], GL_STATIC_DRAW);

		CHECK_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, g_buffer_objects[kFloorSmokeVao][kLifeBuffer]));
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * fsmoke_percent_life.size(), &fsmoke_percent_life[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0); 

		// Use our program.
		CHECK_GL_ERROR(glUseProgram(fsmoke_program_id));

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, smoke_location);

		// Pass uniforms in.
		CHECK_GL_ERROR(glUniformMatrix4fv(fsmoke_projection_matrix_location, 1, GL_FALSE,
					&projection_matrix[0][0]));
		CHECK_GL_ERROR(glUniformMatrix4fv(fsmoke_view_matrix_location, 1, GL_FALSE,
					&view_matrix[0][0]));


		//Draw our triangles.
		CHECK_GL_ERROR(glDrawElementsInstanced(GL_TRIANGLES, fsmoke_faces.size() * 3, GL_UNSIGNED_INT, 0, fsmoke_offsets.size()));

		///////////////// SKYBOX RENDERING //////////////////////////
		// Switch to the skybox VAO.
		CHECK_GL_ERROR(glBindVertexArray(g_array_objects[kSkyBoxVao]));
		
		// Use our program.
		glDepthFunc(GL_LEQUAL);
		CHECK_GL_ERROR(glUseProgram(skybox_program_id));
		glm::mat4 sky_view_matrix = glm::mat4(glm::mat3(g_camera.get_view_matrix()));
		// Pass uniforms in.
		CHECK_GL_ERROR(glUniformMatrix4fv(skybox_projection_matrix_location, 1, GL_FALSE,
					&projection_matrix[0][0]));
		CHECK_GL_ERROR(glUniformMatrix4fv(skybox_view_matrix_location, 1, GL_FALSE,
					&sky_view_matrix[0][0]));

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture_location);
		// Draw our triangles.
		CHECK_GL_ERROR(glDrawElements(GL_TRIANGLES, cube_faces.size() * 3, GL_UNSIGNED_INT, 0));
		glDepthFunc(GL_LESS); // Set depth function back to default

		// Poll and swap.
		glfwPollEvents();
		glfwSwapBuffers(window);

	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}