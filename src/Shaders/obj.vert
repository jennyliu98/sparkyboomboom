
R"zzz(#version 330 core
layout (location = 0) in vec4 vertex_position;
layout (location = 1) in vec3 offset;
layout (location = 2) in float life;
uniform mat4 view;
uniform mat4 projection;
out vec4 tex_position;
out float vs_life;
void main()
{

	vec3 tangent = vec3(view[0][0], view[1][0], view[2][0]);
	vec3 up = vec3(view[0][1], view[1][1], view[2][1]);

	vs_life = life;
	tex_position = vertex_position;
	
	vec3 billboard_pos =  offset.xyz + tangent * tex_position.x + up * tex_position.y;

	billboard_pos.x *= vs_life;
	billboard_pos.y *= vs_life;
	billboard_pos.z *= vs_life;

	gl_Position = projection * view * vec4(billboard_pos, 1);
	
}
)zzz";
