R"zzz(#version 330 core
in vec3 fs_vertex_position;
out vec4 fragment_color;
uniform samplerCube skybox;

void main()
{
	fragment_color = texture(skybox, fs_vertex_position);	
}
)zzz"
