R"zzz(#version 330 core
in vec4 normal;
in vec4 light_direction;
in vec4 world_position;
uniform sampler2D lava;
out vec4 fragment_color;
void main()
{
    float size = 200;
    float tex_x = (world_position.x + size) /(size * 2);
	float tex_y = (world_position.z + size) /(size * 2);
    fragment_color = texture2D(lava, vec2(tex_x, tex_y));
}
)zzz"