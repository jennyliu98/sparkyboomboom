
R"zzz(#version 330 core
layout (location = 0) in vec4 vertex_position;
layout (location = 1) in vec3 offset;
layout (location = 2) in float life;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 light_position;
out vec4 vs_light_direction;
out vec4 vs_vertex_position;
out vec4 texel_position;
out float vs_life;
void main()
{

	vec3 tangent = vec3(view[0][0], view[1][0], view[2][0]);
	vec3 up = vec3(view[0][1], view[1][1], view[2][1]);

	vs_vertex_position = vertex_position;
	texel_position = vertex_position;
	// Scaling to life
	vs_vertex_position.x *= (life) * 3;
	vs_vertex_position.y *= (life);
	

	vs_life = life;
	vec3 billboard_pos =  offset.xyz + tangent * vs_vertex_position.x + up * vs_vertex_position.y;


	
	gl_Position = projection* view * vec4(billboard_pos, 1);

}
)zzz";