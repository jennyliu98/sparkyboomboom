
//http://www.chinedufn.com/webgl-particle-effect-billboard-tutorial/
R"zzz(#version 330 core
in float vs_life;
in vec4 tex_position;
out vec4 fragment_color;
uniform sampler2D fireAtlas;
void main()
{
	vec4 explosion_color = mix(vec4(1, 1, 0, 1), vec4(1, 0, 0, 1), vs_life);

	float offset = floor(16 * vs_life);
	float offsetX = floor(mod(offset, 4.0)) / 4.0;
	float offsetY = floor(offset / 4.0) / 4.0;
	float size = 60;
	float tex_x = (tex_position.x + size) /(size * 2);
	float tex_y = (tex_position.y + size) /(size * 2);
	//Set the frag color to the fragment in the sprite within our texture atlas
	vec4 color = texture2D(fireAtlas, vec2(-((tex_x /4.0) + offsetX), ((tex_y/4.0) + offsetY))) * explosion_color;
	fragment_color = clamp(color, 0.0, 1 - vs_life);
}
)zzz"