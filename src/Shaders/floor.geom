R"zzz(#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;
uniform mat4 projection;
in vec4 te_old_vertex[];
in vec4 te_light_dir[];
in vec4 te_light_direction[];
out vec4 normal;
out vec4 light_direction;
out vec4 world_position;
void main()
{
	int n = 0;
	vec3 a = ( te_old_vertex[1] -  te_old_vertex[0]).xyz;
    vec3 b = (te_old_vertex[2] - te_old_vertex[0]).xyz;
    vec3 N = normalize( cross(a, b) );
	
	for (n = 0; n < gl_in.length(); n++) {
		light_direction = te_light_dir[n];
		gl_Position = projection * gl_in[n].gl_Position;
		normal.xyz = N;
		normal.w = 0;
		world_position = te_old_vertex[n];
		EmitVertex();
	}
	EndPrimitive();
}
)zzz"