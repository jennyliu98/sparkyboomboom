R"zzz(#version 330 core
uniform mat4 projection; // Add projection uniform
uniform mat4 view;

in vec3 vertex_position;
out vec3 fs_vertex_position;

void main()
{
	fs_vertex_position = vertex_position;
	vec4 pos = projection * view * vec4(vertex_position, 1.0);
    gl_Position = pos.xyww; 

}
)zzz"


