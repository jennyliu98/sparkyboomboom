R"zzz(#version 330 core
in vec4 vertex_position;
uniform mat4 view;
uniform vec4 light_position;
out vec4 vs_light_direction;
out vec4 vs_light_dir;
out vec4 vs_old_vertex;
void main()
{
	gl_Position = view * vertex_position;
	vs_light_direction = -gl_Position + view * light_position;
	vs_light_dir = -vertex_position + light_position;
	vs_old_vertex = vertex_position;
}
)zzz"