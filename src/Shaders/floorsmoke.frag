
//http://www.chinedufn.com/webgl-particle-effect-billboard-tutorial/
R"zzz(#version 330 core
in vec4 normal;
in float vs_life;
in vec4 vs_vertex_position;
in vec4 texel_position;
out vec4 fragment_color;
uniform sampler2D smokeAtlas;
void main()
{
	vec4 gray = normalize(vec4(46, 47, 48, 255));
	
	float size = 60;
	float tex_x = (texel_position.x + size) /(size * 2);
	float tex_y = (texel_position.y + size) /(size * 2);

	vec4 color = texture2D(smokeAtlas, vec2(tex_x, -tex_y)) * gray;
	fragment_color = clamp(color, 0.0,  1 - vs_life);
	
	if (vs_life == 0)
		fragment_color = vec4(1,0,0,1);
}
)zzz"